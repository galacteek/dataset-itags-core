itags
=====

This repository contains the yaml-ld definitions for the
RDF tags used by [galacteek](https://gitlab.com/galacteek/galacteek).

The [TAG schema](https://gitlab.com/galacteek/galacteek-ld-web4/-/blob/master/galacteek_ld_web4/contexts/galacteek.ld/Tag.yaml-ld)
is inspired by [MOAT (meaning of a tag)](https://hea-www.harvard.edu/~dburke/vo/astromoat/moat.html).

Contribute
==========

Contributions to the tags dataset are very much welcome !

Contributions should be made to any of the files in the *auto/dbpedia*
directory (the *sink* file is where yet-unclassified tags go).
Examples of tag entries:

```sh
Cannabis
Tennis

-u it:comic -n comic Comic_Book
-p it:sport Football
```

The only mandatory part is the resource name: it should be the name
of a valid dbpedia resource (inside the *http://dbpedia.org/resource*
namespace). Optional options are:

- *-p* specifies the URI of the tag's parent (example: *it:science:biology* has for
  parent: *it:science*)
- *-u* specifies the Tag URI (it should use the *it:* prefix)
- *-n* specifies the Tag's internal name (in english)
- *-s* (small): only gets the resource labels for the english language

The lines starting with *!* (exclamation mark) preset arguments to be
passed to the *itag* command (each *!* call sets the arguments for whatever
follows in the file).

Format
======

The *name* and *displayNames* properties use the *@language*
JSON-LD container and therefore contain a value for each
language.

The *@id* of your *Tag* should use the *it* URI scheme.
Some examples of valid URI identifiers:

- it:ipfs
- it:science:biology
- it:earth:plants

The *name* property is only used internally, while
the *displayName* property is what will be shown in the UI.

The *means* property should be a list of *TagMeaning*. The
*uri* property of *TagMeaning* should be a link to a resource
that describes or is directly related to what the tag is about.

```yaml
'@context': 'ips://galacteek.ld/Tag'
'@graph':
  - '@type': 'Tag'
    '@id': it:article

    name:
      en: article

    displayName:
      en: Article
      fr: Article
      es: Articulo

    means:
      - type: 'TagMeaning'
        uri: 'http://dbpedia.org/resource/Article'
```
